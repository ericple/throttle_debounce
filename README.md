# @pieDesign/libdebounce

> 几乎无需修改原有代码，优雅地节流与防抖

本库包含四个装饰器，功能分别对应为：

- 节流
- 带返回值节流
- 防抖
- 带返回值防抖

如果装饰器未说明需要修改原函数，则可直接使用，若需要修改为异步形式，会特殊说明。

## 安装

```bash
ohpm install @pieDesign/libdebounce
```

## 使用

### 节流

**@Throttle(interval:number)**

该装饰器提供节流效果。

```
import { Throttle } from "@pieDesign/debounce";

@Component
export struct Index {
  @State originDataArray: string[] = [];
  @State filteredDataArray: string[] = [];
  @State searchMode: boolean = false;
  
  // 使用@Throttle装饰器，使search函数节流
  @Throttle(1000)
  search(keyword:string) {
    if(keyword == '') {
      this.filteredDataArray = [];
      return;
    }
    this.filteredDataArray = this.originDataArray.filter(
      originData => originData.includes(str)
    );
  }
  
  // ...
}
```

本示例通过用@Throttle装饰搜索函数，限制用户每秒只能进行一次搜索。

### 防抖

**@Debounce(interval:number)**

该装饰器提供防抖效果。

```
import { Debounce } from "@pieDesign/debounce";

@Component
export struct Index {
  @State originDataArray: string[] = [];
  @State filteredDataArray: string[] = [];
  @State searchMode: boolean = false;
  
  // 使用@Debounce装饰器，使search函数防抖
  @Debounce(500)
  search(keyword:string) {
    if(keyword == '') {
      this.filteredDataArray = [];
      return;
    }
    this.filteredDataArray = this.originDataArray.filter(
      originData => originData.includes(str)
    );
  }
  
  // ...
}
```

### 带返回值的节流

**@ThrottleWithResult(interval: number, leading:boolean = true, trailing:boolean = false)**

> 该装饰器需要将原函数修改为异步形式

修改前：
```
import { ThrottleWithResult } from "@pieDesign/debounce";

@Component
export struct Index {
  @State originDataArray: string[] = [];
  @State filteredDataArray: string[] = [];
  @State searchMode: boolean = false;
  
  search(keyword:string): string {
    if(keyword == '') {
      this.filteredDataArray = [];
      return;
    }
    return this.originDataArray.filter(
      originData => originData.includes(str)
    );
  }
  
  build() {
    Column() {
      Button("Search")
        .onClick(() => {
          this.filteredDataArray = this.search('some str');
        })
    }
  }
}
```

修改后：
```
import { ThrottleWithResult } from "@pieDesign/debounce";

@Component
export struct Index {
  @State originDataArray: string[] = [];
  @State filteredDataArray: string[] = [];
  @State searchMode: boolean = false;
  
  // 使用@ThrottleWithResult装饰器，使search函数节流
  @ThrottleWithResult(1000， true, false)
  async search(keyword:string): string {
    if(keyword == '') {
      this.filteredDataArray = [];
      return;
    }
    return this.originDataArray.filter(
      originData => originData.includes(str)
    );
  }
  
  build() {
    Column() {
      Button("Search")
        .onClick(() => {
          this.search('some str').then(result => {
            this.filteredDataArray = result;
          })
        })
    }
  }
}
```

### 带返回值的防抖

**@DebounceWithResult(interval: number)**

> 该装饰器需要将原函数修改为异步形式

修改前：
```
import { DebounceWithResult } from "@pieDesign/debounce";

@Component
export struct Index {
  @State originDataArray: string[] = [];
  @State filteredDataArray: string[] = [];
  @State searchMode: boolean = false;
  
  search(keyword:string): string {
    if(keyword == '') {
      this.filteredDataArray = [];
      return;
    }
    return this.originDataArray.filter(
      originData => originData.includes(str)
    );
  }
  
  build() {
    Column() {
      Button("Search")
        .onClick(() => {
          this.filteredDataArray = this.search('some str');
        })
    }
  }
}
```

修改后：
```
import { DebounceWithResult } from "@pieDesign/debounce";

@Component
export struct Index {
  @State originDataArray: string[] = [];
  @State filteredDataArray: string[] = [];
  @State searchMode: boolean = false;
  
  // 使用@DebounceWithResult装饰器，使search函数防抖
  @DebounceWithResult(1000)
  async search(keyword:string): string {
    if(keyword == '') {
      this.filteredDataArray = [];
      return;
    }
    return this.originDataArray.filter(
      originData => originData.includes(str)
    );
  }
  
  build() {
    Column() {
      Button("Search")
        .onClick(() => {
          this.search('some str').then(result => {
            this.filteredDataArray = result;
          })
        })
    }
  }
}
```